import com.test.enums.SearchType

layout 'main.tpl',
        pageTitle: 'Index Builder Page',
        mainBody: contents {
            div(class : 'central-input') {
                div(class: 'central-input-inner text-center') {
                    form(id:'search-form', role: 'form', class : 'search', action : 'search', method:'GET') {
                        div(class: 'input-group input-group-lg') {
                            div(class:'col-md-12') {
                                div(class:'col-md-8') {
                                    input(type:'text', name:'q', value: query, placeholder:'Search', class:'form-control')
                                }
                                div(class:'col-md-4') {
                                    select(name:'s', class:'form-control',onchange: '$("#search-form").submit();') {
                                        if (SearchType.RELEVANCE == searchType) {
                                            option(SearchType.RELEVANCE.getLabel(), value: SearchType.RELEVANCE, selected: true)
                                        } else {
                                            option(SearchType.RELEVANCE.getLabel(), value: SearchType.RELEVANCE)
                                        }
                                        if (SearchType.LEXICOGRAPHICAL == searchType) {
                                            option(SearchType.LEXICOGRAPHICAL.getLabel(), value: SearchType.LEXICOGRAPHICAL, selected: true)
                                        } else {
                                            option(SearchType.LEXICOGRAPHICAL.getLabel(), value: SearchType.LEXICOGRAPHICAL)
                                        }
                                    }
                                }
                            }

                            span(class: 'input-group-btn') {
                                button(type: 'submit', class: 'btn btn-primary', value: 'Start indexing') {
                                    text("Search")
                                }

                            }
                        }
                    }
                }
            }
            page.pages.each {
                item ->
                    div(class:'col-md-12 spacer') {
                        a(href: item.url, item.title)
                        br()
                        span(item.highlightedUrl)
                        br()
                        span (item.highlight)
                    }
            }

            if (page.totalPages > 1) {
                boolean lastPage = page.page == page.totalPages;
                int pgStart = Math.max((int)page.page - 10 / 2, (int)1);
                int pgEnd = pgStart + 10;
                if (pgEnd > page.totalPages + 1) {
                    int diff = pgEnd - page.totalPages;
                    pgStart -= diff - 1;
                    if (pgStart < 1)
                        pgStart = 1;
                    pgEnd = page.totalPages + 1;
                }
                ul(class:'pagination') {
                    if (page.page > 1) {
                        li {
                            a(href:'/search?q=' + query + '&p=' + (int)(page.page - 1), 'prev')
                        }
                    }
                    for (int i = pgStart; i < pgEnd; i++) {
                        if (i == page.page)
                            li(class:'active') {
                                a(href:'/search?q=' + query + '&p=' + i, class: 'active', '' + i)
                            }
                        else
                            li {
                                a(href:'/search?q=' + query + '&p=' + i, '' + i)
                            }
                    }
                    if (!lastPage) {
                        li {
                            a(href:'/search?q=' + query + '&p=' + (int)(page.page + 1), 'next')
                        }
                    }
                }
            }
        }