layout 'main.tpl',
        pageTitle: 'Index Builder Page',
        mainBody: contents {
            div(class : 'central-input') {
                div(class: 'central-input-inner text-center') {
                    form(role: 'form', class : 'search', action : 'index', method:'POST') {
                        div {
                            input(type:'text', name:'q', placeholder:'Enter URL', class:'form-control')
                            br()
                            input(type:'number', name:'d', value: maxDepth, placeholder:'Recursion level', class:'form-control small')
                            br()
                            div(class: 'input-group-btn') {
                                button(type: 'submit', class: 'btn btn-primary', style:'float:left', value: 'Start indexing') {
                                    text("Start indexing")
                                }
                            }
                        }

                    }
                }
            }
        }