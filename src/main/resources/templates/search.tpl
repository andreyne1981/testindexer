layout 'main.tpl',
        pageTitle: 'Index Builder Page',
        mainBody: contents {
            div(class : 'central-input') {
                div(class: 'central-input-inner text-center') {
                    form(role: 'form', class : 'search', action : 'search', method:'GET') {
                        div(class: 'input-group input-group-lg') {
                            input(type:'text', name:'q', placeholder:'Search', class:'form-control')
                            span(class: 'input-group-btn') {
                                button(type: 'submit', class: 'btn btn-primary', value: 'Start indexing') {
                                    text("Search")
                                }

                            }
                        }
                    }
                }
            }
        }