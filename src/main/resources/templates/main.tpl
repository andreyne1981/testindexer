yieldUnescaped '<!DOCTYPE html>'
html {
  head {
    title(pageTitle)
    link(rel: 'stylesheet', href: '/css/bootstrap.min.css')
    link(rel: 'stylesheet', href: '/css/indexer.css')
    yieldUnescaped '<script type="text/javascript" src="/js/jquery-1.9.1.js"></script>'
  }
  body {
    div(class: 'container') {
      mainBody()
    }
  }
}