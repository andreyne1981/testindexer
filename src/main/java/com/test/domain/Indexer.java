package com.test.domain;

import com.test.exceptions.IndexerException;

public interface Indexer {

    void index(String url, String title, String body) throws IndexerException;

}
