package com.test.domain;

import com.test.enums.SearchType;
import com.test.exceptions.IndexerException;
import com.test.exceptions.SearcherException;
import com.test.model.Page;
import com.test.model.SearchResult;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.FieldInfo;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryparser.flexible.core.QueryNodeException;
import org.apache.lucene.queryparser.flexible.standard.StandardQueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Locale;

@Component
@DependsOn(value = "indexWriter")
public class LuceneTemplate implements Indexer, Searcher {

    private static final Log log = LogFactory.getLog(LuceneTemplate.class);
    public static final String HIGHLIGHTER_PREFIX = "<span class='highlighted'>";
    public static final String HIGHLIGHTER_SUFFIX = "</span>";
    public static final int MAX_FRAGMENT_SIZE = 256;

    @Autowired
    private IndexWriter indexWriter;
    @Autowired
    private SearcherManager searcherManager;
    @Value(value = "${results.per.page}")
    private Integer resultsPerPage;
    private FieldType termVectorEnabledType;

    @PostConstruct
    public void init() {
        log.info("do init in lucene template");
        termVectorEnabledType = new FieldType();
        termVectorEnabledType.setIndexOptions(FieldInfo.IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
        termVectorEnabledType.setStored(true);
        termVectorEnabledType.setStoreTermVectors(true);
        termVectorEnabledType.setTokenized(true);
        termVectorEnabledType.setStoreTermVectorOffsets(true);
        termVectorEnabledType.setIndexed(true);
    }

    @Override
    public void index(String url, String title, String body) throws IndexerException {
        Document doc = new Document();
        doc.add(new StringField("url", url, Field.Store.YES));
        doc.add(new StringField("title", title, Field.Store.YES));
        doc.add(new TextField("body", body, Field.Store.YES));
        doc.add(new Field("tvbody", body, termVectorEnabledType));
        try {
            indexWriter.addDocument(doc);
        } catch (IOException e) {
            throw new IndexerException(e);
        }
    }

    @Override
    public SearchResult search(String searchString, Integer page, SearchType searchType) throws SearcherException {
        SearchResult result = new SearchResult(page);

        Analyzer luceneAnalyzer = getAnalyzer();
        Query query = getQuery(searchString, luceneAnalyzer);

        SortField.Type sortType = getSortType(searchType);

        IndexSearcher searcher = getSearcher();

        try {
            TopDocs results = searcher.search(query, page * resultsPerPage, new Sort(new SortField("body", sortType),
                    new SortField("title", sortType),
                    new SortField("url", sortType)));
            ScoreDoc[] hits = results.scoreDocs;
            int numTotalHits = results.totalHits;
            result.setTotalPages(numTotalHits / resultsPerPage + (numTotalHits % resultsPerPage == 0 ? 0 : 1));
            int end = Math.min(page * resultsPerPage, results.scoreDocs.length - 1);
            Highlighter highlighter = getHighlighter(query);
            for (int i = (page - 1) * resultsPerPage + 1; i < end + 1; i++) {
                Page p = convertHitToPage(hits[i], luceneAnalyzer, searcher, highlighter);
                result.getPages().add(p);
            }
            result.setCount(result.getPages().size());
        } catch (InvalidTokenOffsetsException e) {
            throw new SearcherException(e);
        } catch (IOException e) {
            throw new SearcherException(e);
        } finally {
            try {
                searcherManager.release(searcher);
            } catch (IOException e) {
                throw new SearcherException(e);
            }
        }
        return result;
    }

    private Page convertHitToPage(ScoreDoc hit, Analyzer luceneAnalyzer, IndexSearcher searcher, Highlighter highlighter) throws IOException, InvalidTokenOffsetsException {
        Page p = new Page();
        Document doc = searcher.doc(hit.doc);
        String url = doc.get("url");
        TokenStream tokenStream = TokenSources.getAnyTokenStream(searcher.getIndexReader(), hit.doc, "url", luceneAnalyzer);
        p.setUrl(url);
        String highlight = highlighter.getBestFragment(tokenStream, url);
        p.setHighlightedUrl(StringUtils.isBlank(highlight) ? url : highlight);
        tokenStream = TokenSources.getAnyTokenStream(searcher.getIndexReader(), hit.doc, "title", luceneAnalyzer);
        String title = doc.get("title");
        highlight = highlighter.getBestFragment(tokenStream, title);
        p.setTitle(StringUtils.isBlank(highlight) ? title : highlight);
        tokenStream = TokenSources.getAnyTokenStream(searcher.getIndexReader(), hit.doc, "tvbody", luceneAnalyzer);
        p.setHighlight(highlighter.getBestFragment(tokenStream, doc.get("tvbody")));
        return p;
    }

    private Highlighter getHighlighter(Query query) {
        Highlighter highlighter;SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter(HIGHLIGHTER_PREFIX, HIGHLIGHTER_SUFFIX);
        highlighter = new Highlighter(htmlFormatter, new QueryScorer(query));
        highlighter.setTextFragmenter(new SimpleFragmenter(MAX_FRAGMENT_SIZE));
        return highlighter;
    }

    private IndexSearcher getSearcher() throws SearcherException {
        try {
            return searcherManager.acquire();
        } catch (IOException e) {
            throw new SearcherException(e);
        }
    }

    private Query getQuery(String searchString, Analyzer analyzer) throws SearcherException {
        try {
            StandardQueryParser luceneQueryParser = getQueryParser(analyzer);
            return luceneQueryParser.parse(searchString, "body");
        } catch (QueryNodeException e) {
            throw new SearcherException(e);
        }
    }

    private SortField.Type getSortType(SearchType searchType) {
        SortField.Type sortType;
        switch (searchType) {
            case RELEVANCE:
                sortType = SortField.Type.SCORE;
                break;
            case LEXICOGRAPHICAL:
                sortType = SortField.Type.STRING;
                break;
            default: {
                sortType = SortField.Type.SCORE;
            }
        }
        return sortType;
    }

    private StandardQueryParser getQueryParser(Analyzer analyzer) {
        return new StandardQueryParser(analyzer);
    }

    private Analyzer getAnalyzer() {
        Locale cl = LocaleContextHolder.getLocale();
        if (cl.getCountry().toLowerCase().equals("ru")) {
            return new RussianAnalyzer();
        } else {
            return new StandardAnalyzer();
        }
    }
}
