package com.test.model;

import java.util.ArrayList;
import java.util.List;

public class SearchResult {

    private List<Page> pages;
    private Integer page;
    private Integer totalPages;
    private Integer count;

    public SearchResult() {
        this(1);
    }

    public SearchResult(Integer page) {
        this.page = page;
        pages = new ArrayList<Page>();
    }

    public List<Page> getPages() {
        return pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "pages=" + pages +
                ", page=" + page +
                ", totalPages=" + totalPages +
                ", count=" + count +
                '}';
    }
}
