package com.test.model;

public class Page {

    private String url;
    private String highlightedUrl;
    private String title;
    private String body;
    private String highlight;

    public String getHighlightedUrl() {
        return highlightedUrl;
    }

    public void setHighlightedUrl(String highlightedUrl) {
        this.highlightedUrl = highlightedUrl;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
