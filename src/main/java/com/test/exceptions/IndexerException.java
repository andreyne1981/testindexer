package com.test.exceptions;

public class IndexerException extends Exception {

    public IndexerException(Throwable cause) {
        super(cause);
    }
}
