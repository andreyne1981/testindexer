package com.test.exceptions;

public class SearcherException extends Exception {

    public SearcherException(Throwable cause) {
        super(cause);
    }
}
