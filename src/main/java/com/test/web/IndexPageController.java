package com.test.web;

import com.test.service.IndexService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexPageController {

    private static final Log log = LogFactory.getLog(IndexPageController.class);

    @Value(value = "${page.max.depth}")
    private Integer pageMaxDepth;
    @Autowired
    private IndexService indexService;


    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String get() {
        return "index";
    }

    @RequestMapping(value = "/index", method = RequestMethod.POST)
    public ModelAndView post(@RequestParam(value = "q") final String address, @RequestParam(value = "d", required = false) final Integer depth) {
        log.info("Start indexing");
        ModelAndView modelAndView = new ModelAndView();
        final int mDepth = null == depth ? pageMaxDepth : depth;
        modelAndView.setViewName("index");
        if (StringUtils.isNotBlank(address)) {
            indexService.addToIndex(address, mDepth);
        }
        modelAndView.addObject("maxDepth", mDepth);
        return modelAndView;
    }

}
