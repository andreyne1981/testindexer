package com.test.web;

import com.test.enums.SearchType;
import com.test.model.SearchResult;
import com.test.service.SearchService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.queryparser.flexible.core.QueryNodeException;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Controller
public class SearchPageController {

    private static final Log log = LogFactory.getLog(SearchPageController.class);
    public static final String RELEVANCE = "Relevance";

    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView welcome() throws InvalidTokenOffsetsException, IOException, QueryNodeException {
        return search("", null, SearchType.RELEVANCE);
    }

    @RequestMapping(value = "/search")
    public ModelAndView search(@RequestParam(value = "q", required = false) String query,
                               @RequestParam(value = "p", required = false) Integer page,
                               @RequestParam(value = "s", required = false) SearchType searchType) throws IOException, InvalidTokenOffsetsException, QueryNodeException {
        ModelAndView modelAndView = new ModelAndView();

        if (StringUtils.isBlank(query)) {
            modelAndView.setViewName("search");
        } else {
            modelAndView.setViewName("search-results");

            SearchResult res = searchService.search(query,
                                                    null == page ? 1 : page,
                                                    null == searchType ? SearchType.RELEVANCE : searchType);
            modelAndView.addObject("query", query);
            modelAndView.addObject("page", res);
        }
        modelAndView.addObject("searchType", searchType);
        modelAndView.addObject("query", query);
        return modelAndView;
    }
}
