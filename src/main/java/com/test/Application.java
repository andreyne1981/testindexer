package com.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.index.ConcurrentMergeScheduler;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.LogByteSizeMergePolicy;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSLockFactory;
import org.apache.lucene.util.Version;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
@EnableAutoConfiguration
@EnableScheduling
@EnableAsync
@ComponentScan
public class Application implements AsyncConfigurer {

    private static final Log log = LogFactory.getLog(Application.class);

    @Value("${lucene.index.dir}")
    private String luceneIndexDir;
    @Autowired
    private FSDirectory luceneDirectory;
    @Autowired
    private Analyzer luceneAnalyzer;
    @Autowired
    private IndexWriter indexWriter;

    @Bean(name = "luceneDirectory")
    public FSDirectory getFSDirectory() throws IOException {
        return FSDirectory.open(new File(luceneIndexDir), new SimpleFSLockFactory());
    }

    @Bean(name = "luceneAnalyzer")
    public Analyzer getLuceneAnalyzer() throws IOException {
        return new RussianAnalyzer();
    }

    @Bean(name = "indexWriter")
    @DependsOn(value = { "luceneAnalyzer", "luceneDirectory"})
    public IndexWriter getIndexWriter() throws IOException {
        log.info("Creating index writer");
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_4_10_4, luceneAnalyzer);
        config.setMergePolicy(new LogByteSizeMergePolicy());
        config.setMergeScheduler(new ConcurrentMergeScheduler());
        IndexWriter writer = new IndexWriter(luceneDirectory, config);
        writer.commit();
        return writer;
    }


    @Bean(name = "searcherManager")
    @DependsOn(value = { "indexWriter"})
    public SearcherManager getSearcherManager() throws IOException {
        log.info("index writer: " + indexWriter);
        return new SearcherManager(indexWriter, true, null);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public Executor getAsyncExecutor() {
        return Executors.newSingleThreadExecutor();
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return null;
    }
}
