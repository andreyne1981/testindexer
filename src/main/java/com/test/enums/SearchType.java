package com.test.enums;

public enum SearchType {

    RELEVANCE("Relevance"),
    LEXICOGRAPHICAL("Lexicographic");

    private String label;

    private SearchType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
