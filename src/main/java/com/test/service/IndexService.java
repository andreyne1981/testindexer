package com.test.service;

public interface IndexService {

    public void addToIndex(String address, int maxDepth);
}
