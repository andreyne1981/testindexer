package com.test.service;

import com.test.domain.Searcher;
import com.test.enums.SearchType;
import com.test.exceptions.SearcherException;
import com.test.model.Page;
import com.test.model.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private Searcher searcher;

    @Override
    public SearchResult search(String searchString, Integer page, SearchType type) {
        try {
            return searcher.search(searchString, page, type);
        } catch (SearcherException e) {
            SearchResult result = new SearchResult();
            result.setPage(page);
            result.setPages(Collections.<Page>emptyList());
            result.setCount(0);
            result.setTotalPages(0);
            return result;
        }
    }
}
