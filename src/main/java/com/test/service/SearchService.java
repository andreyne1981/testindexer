package com.test.service;

import com.test.enums.SearchType;
import com.test.model.SearchResult;

public interface SearchService {

    SearchResult search(String searchString, Integer page, SearchType type);
}
