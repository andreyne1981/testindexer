package com.test.service;

import com.test.domain.Indexer;
import com.test.exceptions.IndexerException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.search.SearcherManager;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Service
public class IndexServiceImpl implements IndexService {

    private static final Log log = LogFactory.getLog(IndexServiceImpl.class);

    @Autowired
    private SearcherManager searcherManager;
    @Autowired
    private Indexer indexer;

    @Async
    @Override
    public void addToIndex(String address, int maxDepth) {
        log.info("Start indexing: " + address);
        Set<String> processedUrls = new HashSet<String>();
        addToIndexInternal(address, processedUrls, 0, maxDepth);
        log.info("Stop indexing: " + address);
    }

    private void addToIndexInternal(String address, Set<String> processedUrls, int currentDepth, int maxDepth) {
        log.info("addToIndexInternal: address - " + address + ", processedUrls count - " + processedUrls.size() + ", currentDepth - " + currentDepth);
        if (StringUtils.isBlank(address)) return;
        String urlWithoutScheme = getUrlWithoutScheme(address.toLowerCase().split("#")[0]);
        if (processedUrls.contains(urlWithoutScheme)) return;

        try {
            Document doc = loadDocument(address);
            if (null == doc) return;
            indexer.index(address, doc.title(), doc.body().text());
            processedUrls.add(urlWithoutScheme);

            if (currentDepth == maxDepth) return;

            Elements links = doc.select("a");
            if (null != links) {
                for (Element link : links) {
                    addToIndexInternal(link.attr("abs:href"), processedUrls, currentDepth + 1, maxDepth);
                }
            }
        } catch (IOException e) {
            log.error("Can't load url: " + address, e);
        } catch (IndexerException e) {
            log.error("Unable to index data", e);
        } catch (Throwable e) {
            log.error("Unexpected error occured while processing address: " + address, e);
        }
    }

    @Scheduled(fixedRate = 1000)
    public void refreshIndexReader() {
        try {
            searcherManager.maybeRefresh();
        } catch (IOException e) {
            log.error("Cannot perform index reader refresh", e);
        }
    }

    private Document loadDocument(String address) throws IOException {
        Connection connection = Jsoup.connect(address);
        connection.ignoreContentType(true)
                .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.122 Safari/534.30")
                .timeout(2000).maxBodySize(10 * 1024*1024)
                .followRedirects(true);
        Connection.Response res = connection.execute();
        if (null == res || null == res.contentType() || !res.contentType().contains("text")) return null;
        Document doc = res.parse();
        doc.normalise();
        doc.body().select("noscript").remove();
        return doc;
    }

    private String getUrlWithoutScheme(String s) {
        String url = s;
        if (url.endsWith("/")) {
            url = url.substring(0, url.length() - 1);
        }
        String[] parts = url.split("://");
        return parts.length == 1 ? url : parts[1];
    }
}
